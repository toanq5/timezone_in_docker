﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using test_docker.Models;
using testdocker.ExtesnsionMethod;
namespace test_docker.Controllers
{
    public class HomeController : Controller
    {
        IConfiguration configuration;
        private readonly ILogger _logger;

        public HomeController(IConfiguration configuration, ILogger<HomeController> logger)
        {
            this.configuration = configuration;
            _logger = logger;
        }

        public IActionResult Index()
        {
            _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetById({ID}) NOT FOUND");

            try
            {
                var timeZone = "North Asia Standard Time";
                ViewBag.TimeZone = DateTime.Now.ConvertTimeZone(timeZone);

            }
            catch (Exception ex)
            {

                ViewBag.TimeZone = ex.Message;
            }
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = configuration.GetValue<string>("TestValue:test");

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
