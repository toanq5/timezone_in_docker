FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY ["test-docker.csproj", ""]
RUN dotnet restore "test-docker.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "test-docker.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "test-docker.csproj" -c Release -o /app

#FROM ubuntu:18.04
RUN apt-get update ;\
    apt-get install -y tzdata

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENV TZ=Asia/Ho_Chi_Minh                                               
ENTRYPOINT ["dotnet", "test-docker.dll"]