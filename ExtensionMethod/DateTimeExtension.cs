﻿using System;
using TimeZoneConverter;

namespace testdocker.ExtesnsionMethod
{
    public static class DateTimeExtension
    {
        public static DateTime ConvertTimeZone(this DateTime time, string timeZone)
        {
            return TimeZoneInfo.ConvertTime(time, TZConvert.GetTimeZoneInfo(timeZone));
        }
    }
}
